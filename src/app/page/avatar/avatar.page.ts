import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.page.html',
  styleUrls: ['./avatar.page.scss'],
})
export class AvatarPage implements OnInit {

  btn = true;
  btn1 = false;
  dataList: any;


  listPerson: Personas[] = [
    {
      avatar: 'assets/avatar.png',
      name: 'Cristian',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore cupiditate voluptatum est ullam illum, ipsam iure sunt numquam officiis maiores voluptas facere reiciendis doloribus at iusto cum voluptate ipsa dolor?'
    },
    {
      avatar: 'assets/avatar1.png',
      name: 'Andres',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore cupiditate voluptatum est ullam illum, ipsam iure sunt numquam officiis maiores voluptas facere reiciendis doloribus at iusto cum voluptate ipsa dolor?'
    },
    {
      avatar: 'assets/avatar2.png',
      name: 'Juan',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore cupiditate voluptatum est ullam illum, ipsam iure sunt numquam officiis maiores voluptas facere reiciendis doloribus at iusto cum voluptate ipsa dolor?'
    },
    {
      avatar: 'assets/avatar3.jpg',
      name: 'Ana',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore cupiditate voluptatum est ullam illum, ipsam iure sunt numquam officiis maiores voluptas facere reiciendis doloribus at iusto cum voluptate ipsa dolor?'
    },
    {
      avatar: 'assets/avatar4.png',
      name: 'Camila',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore cupiditate voluptatum est ullam illum, ipsam iure sunt numquam officiis maiores voluptas facere reiciendis doloribus at iusto cum voluptate ipsa dolor?'
    },
  ]

  constructor() { }

  ngOnInit() {

  }

}

interface Personas {
  avatar: string;
  name: string;
  description: String;
}
