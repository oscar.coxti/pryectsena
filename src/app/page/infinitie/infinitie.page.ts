import { Component, OnInit, ViewChild } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular';

@Component({
  selector: 'app-infinitie',
  templateUrl: './infinitie.page.html',
  styleUrls: ['./infinitie.page.scss'],
})
export class InfinitiePage {
  
  infiniteScroll: IonInfiniteScroll;

  btn = true;
  btn1 = false;
  dataList: any;
  
  constructor() {
    this.dataList = [];
    
    // funcion para mostrar los primeros 20 numeros 
    for (let i = 0; i < 20; i++) {
      //guardamos los primeros 20 numeros en el arreglo dataList
      this.dataList.push("Primeros 20 Numeros " + this.dataList.length);
    }
    console.log(this.dataList);
   }

  //evento para cargar el infinitie scroll
  loadData(event) {
    console.log("viene event");

    console.log(event);

    console.log("Valor del Infinitie Scroll", this.infiniteScroll.disabled);
    // funcion para realizar la espera 
    setTimeout(() => {

      // funcion para continuar con los siguientes 20
      for (let i = 0; i < 20; i++) {
        this.dataList.push("Numeros " + this.dataList.length);
      }
      //guardamos los 20 numeros siguientes en el arreglo dataList
      console.log(this.dataList);
      event.target.complete();

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      console.log(this.dataList.length);
      if (this.dataList.length == 200) {
        event.target.disabled = true;
        alert("ya no hay mas datos para mostrar");
      }
    }, 500);
  }

  // funcion para detener el infinitie Scroll
  toggleInfiniteScroll() {
    console.log("Cambio de estado en el Infinitie Scroll");
    if (this.infiniteScroll.disabled == false) {
      this.btn = false;
      this.btn1 = true;
      console.log("Inhabilito el Scroll");
      this.infiniteScroll.disabled = true;
    } else {
      console.log("Habilito el Scroll");
      this.btn1 = false;
      this.btn = true;
      this.infiniteScroll.disabled = false;
    }

    //Otra forma de habilitar y deshabilitar el infinitie scroll
    // this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }
}