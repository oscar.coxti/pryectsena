import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InfinitiePageRoutingModule } from './infinitie-routing.module';

import { InfinitiePage } from './infinitie.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InfinitiePageRoutingModule
  ],
  declarations: [InfinitiePage]
})
export class InfinitiePageModule {}
