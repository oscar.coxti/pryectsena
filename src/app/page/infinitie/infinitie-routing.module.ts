import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InfinitiePage } from './infinitie.page';

const routes: Routes = [
  {
    path: '',
    component: InfinitiePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InfinitiePageRoutingModule {}
