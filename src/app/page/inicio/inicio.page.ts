import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {
  componentes : Componente[] =[
    {
      icon: 'logo-github',
      name: 'GitHub',
      redirectTo: '/action-sheet'
    },
    {
      icon: 'ios-calendar',
      name: 'Calendario',
      redirectTo: '/alert'
    },
    {
      icon: 'card',
      name: 'Avatar',
      redirectTo: '/avatar'
    },
    {
      icon: 'ios-list',
      name: 'Infinitie Scroll',
      redirectTo: '/infinitie'
    },
    {
      icon: 'person',
      name: 'Range',
      redirectTo: '/range'
    },
    {
      icon: 'radio-button-on',
      name: 'Buttons',
      redirectTo: '/buttons'
    },
  ]

  constructor() { }

  ngOnInit() {
  }

}

interface Componente {
  icon: string;
  name: string;
  redirectTo: string;
}

