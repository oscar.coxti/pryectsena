import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'inicio', pathMatch: 'full' },
  {path: 'inicio', loadChildren: () => import('./page/inicio/inicio.module').then( m => m.InicioPageModule)},
  {
    path: 'alert',
    loadChildren: () => import('./page/alert/alert.module').then( m => m.AlertPageModule)
  },
  {
    path: 'action-sheet',
    loadChildren: () => import('./page/action-sheet/action-sheet.module').then( m => m.ActionSheetPageModule)
  },   {
    path: 'avatar',
    loadChildren: () => import('./page/avatar/avatar.module').then( m => m.AvatarPageModule)
  },
  {
    path: 'infinitie',
    loadChildren: () => import('./page/infinitie/infinitie.module').then( m => m.InfinitiePageModule)
  },
  {
    path: 'range',
    loadChildren: () => import('./page/range/range.module').then( m => m.RangePageModule)
  },
  {
    path: 'buttons',
    loadChildren: () => import('./page/buttons/buttons.module').then( m => m.ButtonsPageModule)
  },
 
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
