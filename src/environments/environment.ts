// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyB9Znko673Ke94EJyO7fsafOvXB1_0EFIo",
    authDomain: "resistenciapp-6cedd.firebaseapp.com",
    databaseURL: "https://resistenciapp-6cedd.firebaseio.com",
    projectId: "resistenciapp",
    storageBucket: "resistenciapp.appspot.com",
    messagingSenderId: "19573951950",
    appId: "1:19573951950:web:dbe8980bec1332da486f9f",
    measurementId: "G-D2B0XDM4PZ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
